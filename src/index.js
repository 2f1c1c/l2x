import express from 'express';
var bigInt = require("big-integer");

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.get('/*', function(req, res) { 
 
  let n = +req.query['i'];
  n++;
  let p = [];
  p[0] = 1;
  p[1] = 6*3*p[0];
  p[2] = 6*2*p[1] + 9*3*p[0];
  let i = 3;
  for (; i < n; ++i) {
	  let a = bigInt(p[i-1]).multiply(12)
	  let b = bigInt(p[i-2]).multiply(18);
	  p[i] = bigInt(a).plus(b);  
  }
 
  res.send(p[n-1].toString());
  
});


app.listen(3000, function() {
});




